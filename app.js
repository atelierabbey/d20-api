const express = require('express');
const app = express();
const morgan = require('morgan');


app.use(morgan('dev'));

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'GET')
	}
	next();
});


const v1 = require('./api/v1/default');
app.use('/v1', v1);


app.use((req, res, next) => {
	const error = new Error('Route not found');
	error.status = 404;

	next(error);
});

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		message: error.message

	});
});

module.exports = app;