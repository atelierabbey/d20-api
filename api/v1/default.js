const express = require('express');
const router = express.Router();

const { DiceRoll } = require('rpg-dice-roller');


router.get('/', (req, res, next) => {

	const roll = '1d20';
	const roller = new DiceRoll(roll);

	res.status(200).json({
		result : roller.total,
		input  : roller.notation,
		output : roller.output
	});
});


router.get('/:diceNotation', (req, res, next) => {

	const roll =  req.params.diceNotation;

	const roller = new DiceRoll(roll);

	res.status(200).json({
		result : roller.total,
		input  : roller.notation,
		output : roller.output
	});
});

module.exports = router;